# grid-flex-exemple

Templates de exemplo para a abordagem de questões mais usadas de HTML5, CSS.

Foco de aprendizado desse template é para o entendimento da utilização do CSS FlexBox e CSS Grid.


Links para estudos mais afundo:
 - https://css-tricks.com/snippets/css/a-guide-to-flexbox/
 - https://css-tricks.com/snippets/css/complete-guide-grid/
 - https://www.youtube.com/watch?v=7kVeCqQCxlk -> CSS Grid Changes EVERYTHING 
